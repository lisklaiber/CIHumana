import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';


const APPROUTES: Routes = [
    { path: '', component: AppComponent },
    { path: '**', component: AppComponent }
];

export const appRoutingProviders: any[] = [];
export const ROUTING: ModuleWithProviders = RouterModule.forRoot(APPROUTES, { useHash: false });
